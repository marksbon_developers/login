<?php if(isset($_SESSION['user']['id'])) : ?>

<!-- ***************************** Universal In System *********************************** -->
  <!-- *********** Delete Modal *********** -->
    <script type="text/javascript">
      $(document).on("click",".delete_button",function(){
        $('#deletename_').text($(this).data('deletename'));
        $('.delete_confirmed_').attr('data-formurl',$(this).data('formurl'));
        $('.delete_confirmed_').attr('data-deleteid',$(this).data('deleteid'));
        $('.delete_confirmed_').attr('data-tableid',$(this).data('tableid'));
        $('.delete_confirmed_').attr('data-keyword',$(this).data('keyword'));
        $('#delete_modal_').modal('show');
      });
    </script>
    <div id="delete_modal_" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-danger">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h6 class="modal-title">Delete Confirmation</h6>
          </div>
          <div class="modal-body">
            Do You Want To Really Delete <?php echo "<strong><em id='deletename_'></em></strong>"; ?> .... ?
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button> 
            <button type="button" class="btn btn-danger delete_confirmed_" data-dismiss="modal">Delete</button>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      $(document).on("click",".delete_confirmed_",function(){
        let formurl = $(this).data('formurl');
        let tableid = $(this).data('tableid');
        let formData = { 
          'id': $(this).data('deleteid'),
          'delete_item': "Confirmed",
          'tbl_ref': $(this).data('keyword'),
        };
        ajax_post(formurl,formData,tableid);
      });
    </script>

    <script type="text/javascript">
      $(document).on("click",".delete_btn",function(){
        $('#deletename').text($(this).data('displayname'));
        $('.delete_confirmed').attr('data-user_id',$(this).data('dataid'));
        $('.delete_confirmed').attr('data-email',$(this).data('email'));
        $('.delete_confirmed').attr('data-status',$(this).data('state'));
        $('#delete_modal').modal('show');
      });
    </script>
    <div id="delete_modal" class="modal fade">
      <div class="modal-dialog" style="width: 500px">
        <div class="modal-content">
          <div class="modal-header bg-danger">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h6 class="modal-title">Delete Confirmation</h6>
          </div>
          <div class="modal-body">
            Do You Want To Really Delete <?php echo "<strong><em id='deletename'></em></strong>"; ?> .... ?
            <input type="hidden" id="deleteId" name="deleteid"/> 
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger delete_confirmed" data-dismiss="modal">Delete</button>
          </div>
        </div>
      </div>
    </div>
  <!-- *********** Delete Modal *********** -->

  

<?php endif; ?>