<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller 
{
  /**************** Important ** ********************/
    /*******************************
      Constructor 
    *******************************/
      public function __construct() {
        parent::__construct();
      }

    /*******************************
      Index Function
    *******************************/
      public function index() {
        /*if(isset($_SESSION['user']['username']) && isset($_SESSION['user']['roles']))
          redirect('dashboard');
        else
        {*/
          $title['title'] = "Dashboard"; 
          //$this->load->view('header',$title); 
          $this->load->view('dashboard'); 
          //$this->load->view('views/footer'); 
          //$this->load->view('globals/notifications'); 
        /*}*/
      }
  /**************** Important ** ********************/

}//End of Class
