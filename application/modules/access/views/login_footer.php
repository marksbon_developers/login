        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
          <div class="text-center d-lg-none w-100">
            <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
              <i class="icon-unfold mr-2"></i>
              Footer
            </button>
          </div>

          <div class="navbar-collapse collapse" id="navbar-footer">
            <span class="navbar-text p-0">
              &copy; 2018 - 2019. <a href="#"><em>STRSC Media</em> | </a> <em>Powered by <a href="https://marksbon.com" target="_blank">Marksbon Limited</a></em>
            </span>

            <ul class="navbar-nav ml-lg-auto">
              <!-- <li class="nav-item"><a href="javascript:void" class="navbar-nav-link new_ticket" style="padding: .4rem 1rem"><span class="text-teal-400"><i class="icon-stack2 mr-2"></i></span> New Ticket</a></li> -->
              
              <li class="nav-item"><a href="javascript:void" class="navbar-nav-link create_ticket" style="padding: .4rem 1rem"><i class="icon-lifebuoy mr-2"></i> Support</a></li>

              <li class="nav-item"><a href="javascript:void" class="navbar-nav-link font-weight-semibold license" style="padding: .4rem 1rem"><span class="text-pink-400"><i class="icon-certificate mr-2"></i> License</span></a></li>
            </ul>
          </div>
        </div>
        <!-- /footer -->

        <!-- ****** New Request Modal  ******* --
          <script type="text/javascript">
            $(".new_ticket").click(function(){
              $('#new_request').modal('show');
            });
          </script>
          
          <div id="new_request" class="modal fade">
            <div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header bg-success">
									<h6 class="modal-title">New Request</h6>
									<button type="button" class="close" data-dismiss="modal">×</button>
								</div>

								<div class="modal-body">
									<h6 class="font-weight-semibold">Text in a modal</h6>
									<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>

									<hr>

									<h6 class="font-weight-semibold">Another paragraph</h6>
									<p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
									<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
									<button type="button" class="btn bg-success legitRipple">Save changes</button>
								</div>
							</div>
						</div>
          </div>
        <!-- ****** New Request Modal ******* -->

        <!-- ****** Support Modal  ******* -->
          <script type="text/javascript">
            $(".create_ticket").click(function(){
              $('#create_ticket_modal').modal('show');
            });
          </script>
          
          <div id="create_ticket_modal" class="modal fade">
            <div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header bg-success">
									<h6 class="modal-title">New Support Ticket</h6>
									<button type="button" class="close" data-dismiss="modal">×</button>
								</div>

								<div class="modal-body">
									<h6 class="font-weight-semibold">Text in a modal</h6>
									<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>

									<hr>

									<h6 class="font-weight-semibold">Another paragraph</h6>
									<p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
									<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
									<button type="button" class="btn bg-success legitRipple">Save changes</button>
								</div>
							</div>
						</div>
          </div>
        <!-- ****** Support Modal ******* -->

        <!-- ****** License Modal  ******* -->
          <script type="text/javascript">
            $(".license").click(function(){
              $('#license_modal').modal('show');
            });
          </script>
          
          <div id="license_modal" class="modal fade">
            <div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header bg-teal">
									<h6 class="modal-title">License</h6>
									<button type="button" class="close" data-dismiss="modal">×</button>
								</div>

								<div class="modal-body">
									<h6 class="font-weight-semibold">Text in a modal</h6>
									<p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>

									<hr>

									<h6 class="font-weight-semibold">Another paragraph</h6>
									<p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
									<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link legitRipple" data-dismiss="modal">Close</button>
									<button type="button" class="btn bg-success legitRipple">Save changes</button>
								</div>
							</div>
						</div>
          </div>
        <!-- ****** License Modal ******* -->
      </div>
      <!-- /main content -->
    </div>
    <!-- /page content -->
  </body>
</html>
